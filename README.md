# Lotek

Sprawdź się w lotku! Wybierz swoje szczęśliwe liczby i porównaj trafione numery spośród innych graczy.

## Funkcjonalności

- Generowanie losowych liczb do loterii
- Tworzenie zdefiniowanego gracza
- Tworzenie losowych graczy
- Obliczanie nagród dla zwycięzców na podstawie akumulacji i stopni
- Zapisywanie wyników gier
- Możliwość uruchomienia w trybie konsolowym lub nieinteraktywnym

## Uruchomienie

Aby uruchomić projekt, należy wykonać następujące kroki:

- Pobrać lub sklonować repozytorium
- Uruchomić projekt za pomocą komendy: ``mvn exec:java`` lub skryptu `run-with-arguments.cmd`

## Wymagania
- Java w wersji 11
- Maven w wersji minimum 3.7

## Konfiguracja

Konfiguracja projektu może być dokonana za pomocą skryptu `run-with-arguments.cmd`. Możliwe opcje konfiguracji to:

- ``interface`` - typ interfejsu, możliwe wartości: ``console``, ``non_interactive`` (domyślnie ``console``)
- ``players`` - liczba graczy (domyślnie 1000)
- ``length`` - liczba wybieranych liczb przez graczy (domyślnie 6)
- ``maxNumber`` - maksymalna liczba do wybrania w ramach loterii (domyślnie 49)
- ``accumulation`` - pula nagród (domyślnie 30 000 000 PLN)
- ``grade1`` - procent puli dla nagrody 1 stopnia (domyślnie 43%)
- ``grade2`` - procent puli dla nagrody 2 stopnia (domyślnie 8%)
- ``grade3`` - procent puli dla nagrody 3 stopnia (domyślnie 13%)
- ``grade4`` - procent puli dla nagrody 4 stopnia (domyślnie 36%)
