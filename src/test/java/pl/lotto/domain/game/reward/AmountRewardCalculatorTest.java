package pl.lotto.domain.game.reward;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import pl.lotto.domain.player.Player;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AmountRewardCalculatorTest {
    private static final String LOTTO_ID = "id";
    private static final Map<Grade, Money> gradeAccumulationRewards = Map.of(
            Grade.GRADE_1, Money.of(12_900_000, "PLN"),
            Grade.GRADE_2, Money.of(2_400_000, "PLN"),
            Grade.GRADE_3, Money.of(3_900_000, "PLN"),
            Grade.GRADE_4, Money.of(10_800_000, "PLN")
    );
    private final AmountRewardCalculator amountRewardCalculator = new AmountRewardCalculator(gradeAccumulationRewards);

    @Test
    void when_playerGuessingNumbers_shouldReturnRewards() {
        //given
        List<Player> players = createDefaultPlayers();
        players.add(createPlayer(1, 2, 3, 4, 5, 6));
        List<Integer> guessedNumbersByPlayer = List.of(1, 2, 3, 4, 5, 6);
        //when
        List<GameReward> rewards = amountRewardCalculator.rewards(players, guessedNumbersByPlayer, LOTTO_ID);
        //then
        assertEquals(1, rewards.size());
    }

    @Test
    void when_playerGuessingLessThan3Numbers_shouldNotReturnReward() {
        //given
        List<Player> players = createDefaultPlayers();
        players.add(createPlayer(1, 2, 3, 4, 5, 6));
        List<Integer> guessedNumbersByPlayer = List.of(5, 6, 7, 8, 9, 10);
        //when
        List<GameReward> rewards = amountRewardCalculator.rewards(players, guessedNumbersByPlayer, LOTTO_ID);
        //then
        assertEquals(0, rewards.size());
    }

    @Test
    void when_playerGuessingGrade1Numbers_itShouldReturnGradeAccumulation() {
        //given
        List<Player> players = createDefaultPlayers();
        players.add(createPlayer(1, 2, 3, 4, 5, 6));
        List<Integer> guessedGrade1ByPlayer = List.of(1, 2, 3, 4, 5, 6);
        Money grade1ExpectedMoney = gradeAccumulationRewards.get(Grade.GRADE_1);
        //when
        List<GameReward> rewards = amountRewardCalculator.rewards(players, guessedGrade1ByPlayer, LOTTO_ID);
        //then
        assertEquals(1, rewards.size());
        assertEquals(grade1ExpectedMoney, rewards.get(0).getReward());
    }

    @Test
    void when_2playersGuessingGrade1Numbers_itShouldReturnGradeAccumulation_andBeSplitByPlayers() {
        //given
        int expectedPlayersRewards = 2;
        List<Player> players = createDefaultPlayers();
        players.add(createPlayer(1, 2, 3, 4, 5, 6));
        players.add(createPlayer(1, 2, 3, 4, 5, 6));
        List<Integer> guessedGrade1ByPlayer = List.of(1, 2, 3, 4, 5, 6);
        Money grade1ExpectedMoney = gradeAccumulationRewards.get(Grade.GRADE_1).divide(expectedPlayersRewards);
        //when
        List<GameReward> rewards = amountRewardCalculator.rewards(players, guessedGrade1ByPlayer, LOTTO_ID);
        //then
        assertEquals(expectedPlayersRewards, rewards.size());
        assertEquals(grade1ExpectedMoney, rewards.get(0).getReward());
        assertEquals(grade1ExpectedMoney, rewards.get(1).getReward());
    }

    private Player createPlayer(Integer... numbers) {
        return new Player("firstname", "lastname", Arrays.asList(numbers));
    }

    private List<Player> createDefaultPlayers() {
        List<Player> players = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            players.add(createPlayer(1, 1, 1, 1, 1, 1));
        }
        return players;
    }
}