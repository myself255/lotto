package pl.lotto.domain.game;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import pl.lotto.domain.game.result.GameResult;
import pl.lotto.domain.game.reward.AmountRewardCalculator;
import pl.lotto.domain.game.reward.Grade;
import pl.lotto.domain.player.Player;
import pl.lotto.domain.randomizer.RandomNumbersGenerator;
import pl.lotto.infrastructure.randomize.MathRandomNumber;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LottoGameTest {
    private static final Clock DEFAULT_CLOCK = Clock.systemDefaultZone();
    private static final Map<Grade, Money> gradeAccumulationRewards = Map.of(
            Grade.GRADE_1, Money.of(12_900_000, "PLN"),
            Grade.GRADE_2, Money.of(2_400_000, "PLN"),
            Grade.GRADE_3, Money.of(3_900_000, "PLN"),
            Grade.GRADE_4, Money.of(10_800_000, "PLN")
    );
    private final AmountRewardCalculator amountRewardCalculator = new AmountRewardCalculator(gradeAccumulationRewards);
    private final RandomNumbersGenerator randomNumbersGenerator =
            new RandomNumbersGenerator(new MathRandomNumber(), 40);
    private final List<Player> players = new ArrayList<>(List.of(new Player("firstname", "lastName",
            List.of(1, 1, 1, 1, 1, 1))));

    @Test
    void when_numbersToSelectIs6_itShouldGenerate6Numbers() {
        //when
        GameResult gameResult = defaultLottoGame(6).run(players);
        //then
        assertEquals(6, gameResult.getNumbers().size());
    }

    @Test
    void when_numbersToSelectIs7_itShouldGenerate6Numbers() {
        //when
        GameResult gameResult = defaultLottoGame(7).run(players);
        //then
        assertEquals(7, gameResult.getNumbers().size());
    }

    @Test
    void when_playerGuessingNumbers_itShouldGeneratePlayersReward() {
        //given
        Player winner = new Player("firstname", "lastname", List.of(4, 5, 6, 7, 8, 9));
        players.add(winner);
        //when
        GameResult gameResult = lottoGameWithDefinedGeneratedNumbers().run(players);
        //then
        assertTrue(gameResult.getRewards().size() >= 1);
        assertEquals(winner, gameResult.getRewards().get(0).getPlayer());
    }

    private LottoGame defaultLottoGame(int numbersToSelect) {
        return new LottoGame("id", numbersToSelect, randomNumbersGenerator, amountRewardCalculator, DEFAULT_CLOCK);
    }

    private LottoGame lottoGameWithDefinedGeneratedNumbers() {
        AtomicInteger nextNumber = new AtomicInteger(4);
        RandomNumbersGenerator customGenerator =
                new RandomNumbersGenerator((min, max) -> nextNumber.getAndIncrement(), 50);
        return new LottoGame("id", 6, customGenerator, amountRewardCalculator, DEFAULT_CLOCK);
    }
}