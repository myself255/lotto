package pl.lotto.domain.randomizer;

import org.junit.jupiter.api.Test;
import pl.lotto.infrastructure.randomize.MathRandomNumber;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RandomNumbersGeneratorTest {
    public RandomNumbersGenerator randomNumbersGenerator =
            new RandomNumbersGenerator(new MathRandomNumber(), 50);

    @Test
    void when_numbersAmountIs6_itShouldGenerate6Numbers() {
        //given
        int numbersAmount = 6;
        //when
        List<Integer> numbers = randomNumbersGenerator.generateUnique(numbersAmount);
        //then
        assertEquals(numbersAmount, numbers.size());
    }

    @Test
    void when_numbersAmountIs7_itShouldGenerate7Numbers() {
        //given
        int numbersAmount = 7;
        //when
        List<Integer> numbers = randomNumbersGenerator.generateUnique(numbersAmount);
        //then
        assertEquals(numbersAmount, numbers.size());
    }

    @Test
    void numberGenerator_shouldGenerateUniqueNumbers() {
        //given
        randomNumbersGenerator =
                new RandomNumbersGenerator(new MathRandomNumber(), 6);
        //when
        List<Integer> numbers = randomNumbersGenerator.generateUnique(6);
        numbers.sort(Integer::compareTo);
        //then
        assertEquals(List.of(1, 2, 3, 4, 5, 6), numbers);
    }
}