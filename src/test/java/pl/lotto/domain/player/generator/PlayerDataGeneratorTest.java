package pl.lotto.domain.player.generator;

import org.junit.jupiter.api.Test;
import pl.lotto.domain.player.Player;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PlayerDataGeneratorTest {
    private final static int MAX_NUMBER_BOUND = 49;

    @Test
    void when_givenPlayerLimit100_itShouldReturn100Players() {
        //given
        int playerLimit = 100;
        //when
        List<Player> players =
                PlayerDataGenerator.generateRandomPersons(playerLimit, 6, MAX_NUMBER_BOUND);
        //then
        assertEquals(playerLimit, players.size());
    }

    @Test
    void when_givenNumberLength7_itShouldReturn7GeneratedNumbers() {
        //given
        int playerLimit = 1;
        int numberLength = 7;
        //when
        List<Player> players =
                PlayerDataGenerator.generateRandomPersons(playerLimit, numberLength, MAX_NUMBER_BOUND);
        //then
        assertEquals(playerLimit, players.size());
        assertEquals(numberLength, players.get(0).getSelectedNumbers().size());
    }

    @Test
    void when_generatedPlayer_itShouldFillAllFields() {
        //given
        int playerLimit = 1;
        int numberLength = 6;
        //when
        List<Player> players =
                PlayerDataGenerator.generateRandomPersons(playerLimit, numberLength, MAX_NUMBER_BOUND);
        //then
        assertEquals(playerLimit, players.size());
        assertNotNull(players.get(0));
        assertNotNull(players.get(0).getFirstname());
        assertTrue(players.get(0).getFirstname().length() > 0);
        assertNotNull(players.get(0).getLastname());
        assertTrue(players.get(0).getLastname().length() > 0);
        assertNotNull(players.get(0).getSelectedNumbers());
        assertTrue(players.get(0).getSelectedNumbers().size() > 0);
    }
}