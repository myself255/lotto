package pl.lotto.infrastructure.builder.lotto;

import org.javamoney.moneta.Money;
import pl.lotto.domain.game.executor.LottoGameExecutor;
import pl.lotto.domain.game.reward.AmountRewardCalculator;
import pl.lotto.domain.game.reward.Grade;
import pl.lotto.domain.player.Player;
import pl.lotto.domain.game.executor.ConsoleLottoGameExecutor;
import pl.lotto.infrastructure.game.InMemoryGameResultStorage;
import pl.lotto.domain.game.executor.NonInteractiveLottoGameExecutor;
import pl.lotto.domain.player.generator.PlayerDataGenerator;
import pl.lotto.infrastructure.randomize.MathRandomNumber;
import pl.lotto.ui.console.ConsoleUI;

import java.time.Clock;
import java.util.List;
import java.util.Map;

public class LottoBuilder {

    public static ChooseInterfaceType builder(Clock clock) {
        return new Builder(clock);
    }

    public static class Builder implements ChooseInterfaceType, NeedNumberToSelect, NeedNumberMaxLimit,
            NeedAccumulation, NeedGradesRatio, CanBeBuild {
        private final Clock clock;
        private int amountOfPlayers = 1000;
        private InterfaceType interfaceType;
        private int numbersLength;
        private int maxNumberLimit;
        private Money accumulation;
        private double grade1;
        private double grade2;
        private double grade3;
        private double grade4;

        public Builder(Clock clock) {
            this.clock = clock;
        }

        @Override
        public CanBeBuild selectAmountOfPlayers(int players) {
            this.amountOfPlayers = players;
            return this;
        }

        @Override
        public LottoGameExecutor build() {
            List<Player> players =
                    PlayerDataGenerator.generateRandomPersons(amountOfPlayers, numbersLength, maxNumberLimit);
            Map<Grade, Money> accumulationGrades = Map.of(
                    Grade.GRADE_1, accumulation.multiply(grade1),
                    Grade.GRADE_2, accumulation.multiply(grade2),
                    Grade.GRADE_3, accumulation.multiply(grade3),
                    Grade.GRADE_4, accumulation.multiply(grade4)
            );
            LottoGameExecutor lottoGameExecutor;
            switch (interfaceType) {
                case CONSOLE:
                    lottoGameExecutor = new ConsoleLottoGameExecutor(new InMemoryGameResultStorage(),
                            numbersLength, clock, new MathRandomNumber(), maxNumberLimit,
                            new AmountRewardCalculator(accumulationGrades), players, new ConsoleUI());
                    break;
                case NON_INTERACTIVE:
                default:
                    lottoGameExecutor = new NonInteractiveLottoGameExecutor(new InMemoryGameResultStorage(),
                            numbersLength, clock, new MathRandomNumber(), maxNumberLimit,
                            new AmountRewardCalculator(accumulationGrades), players);
                    break;
            }
            return lottoGameExecutor;
        }

        @Override
        public NeedNumberToSelect nonInteractive() {
            this.interfaceType = InterfaceType.NON_INTERACTIVE;
            return this;
        }

        @Override
        public NeedNumberToSelect console() {
            this.interfaceType = InterfaceType.CONSOLE;
            return this;
        }

        @Override
        public NeedNumberToSelect selectInterfaceType(String interfaceType) {
            this.interfaceType = InterfaceType.valueOf(interfaceType.toUpperCase());
            return this;
        }

        @Override
        public NeedNumberMaxLimit selectGeneratedNumberLength(int number) {
            this.numbersLength = number;
            return this;
        }

        @Override
        public NeedAccumulation selectMaxNumberLimit(int number) {
            this.maxNumberLimit = number;
            return this;
        }

        @Override
        public NeedGradesRatio selectAccumulation(Money accumulation) {
            this.accumulation = accumulation;
            return this;
        }

        @Override
        public CanBeBuild selectGradesRatio(double grade1, double grade2, double grade3, double grade4) {
            this.grade1 = grade1;
            this.grade2 = grade2;
            this.grade3 = grade3;
            this.grade4 = grade4;
            return this;
        }
    }

    public interface ChooseInterfaceType {
        NeedNumberToSelect nonInteractive();

        NeedNumberToSelect console();

        NeedNumberToSelect selectInterfaceType(String interfaceType);
    }

    public interface NeedNumberToSelect {
        NeedNumberMaxLimit selectGeneratedNumberLength(int number);
    }

    public interface NeedNumberMaxLimit {
        NeedAccumulation selectMaxNumberLimit(int number);
    }

    public interface NeedAccumulation {
        NeedGradesRatio selectAccumulation(Money accumulation);
    }

    public interface NeedGradesRatio {
        CanBeBuild selectGradesRatio(double grade1, double grade2, double grade3, double grade4);
    }

    public interface CanBeBuild {
        CanBeBuild selectAmountOfPlayers(int players);

        LottoGameExecutor build();
    }
}
