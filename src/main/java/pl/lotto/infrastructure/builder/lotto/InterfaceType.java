package pl.lotto.infrastructure.builder.lotto;

public enum InterfaceType {
    CONSOLE, NON_INTERACTIVE
}
