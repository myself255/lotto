package pl.lotto.infrastructure;

import java.util.HashMap;
import java.util.Map;

public enum Argument {
    INTERFACE("interface", "console"),
    LENGTH("length", "6"),
    LIMIT("limit", "49"),
    ACCUMULATION("accumulation", "30000000"),
    GRADE1("grade1", "0.43"),
    GRADE2("grade2", "0.08"),
    GRADE3("grade3", "0.13"),
    GRADE4("grade4", "0.36"),
    PLAYERS("players", "1000");

    private final String name;
    private final String defaultValue;

    Argument(String name, String defaultValue) {
        this.name = name;
        this.defaultValue = defaultValue;
    }

    public String getName() {
        return name;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public static Map<Argument, String> parseArguments(String[] args) {
        Map<Argument, String> parsedArgs = new HashMap<>();
        for (Argument argument : Argument.values()) {
            parsedArgs.put(argument, argument.getDefaultValue());
        }

        for (int i = 0; i < args.length; i++) {
            String argName = args[i];
            if (argName.startsWith("-")) {
                argName = argName.substring(1);
                for (Argument argument : Argument.values()) {
                    if (argument.getName().equals(argName) && args.length > i + 1) {
                        parsedArgs.put(argument, args[i + 1]);
                        break;
                    }
                }
            }
        }
        return parsedArgs;
    }
}
