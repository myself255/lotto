package pl.lotto.infrastructure.randomize;

import pl.lotto.domain.randomizer.RandomNumber;

public class MathRandomNumber implements RandomNumber {

    @Override
    public int randomize(int min, int max) {
        max++;
        return (int) ((Math.random() * (max - min)) + min);
    }
}
