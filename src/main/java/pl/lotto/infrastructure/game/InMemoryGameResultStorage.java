package pl.lotto.infrastructure.game;

import pl.lotto.domain.game.result.GameResult;
import pl.lotto.domain.game.result.GameResultStorage;

import java.util.ArrayList;
import java.util.List;

public class InMemoryGameResultStorage implements GameResultStorage {
    private final List<GameResult> gameResults = new ArrayList<>();

    @Override
    public void save(GameResult gameResult) {
        gameResults.add(gameResult);
    }
}
