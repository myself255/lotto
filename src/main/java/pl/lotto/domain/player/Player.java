package pl.lotto.domain.player;

import java.util.List;

public class Player {
    private final String firstname;
    private final String lastname;
    private final List<Integer> selectedNumbers;

    public Player(String firstname, String lastname, List<Integer> selectedNumbers) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.selectedNumbers = selectedNumbers;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public List<Integer> getSelectedNumbers() {
        return selectedNumbers;
    }
}
