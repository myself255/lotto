package pl.lotto.domain.player.generator;

import pl.lotto.domain.player.Player;
import pl.lotto.domain.randomizer.RandomNumbersGenerator;
import pl.lotto.infrastructure.randomize.MathRandomNumber;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PlayerDataGenerator {
    private static final List<String> FIRST_NAMES = List.of("Aureliusz", "Arkadiusz", "Patryk", "Oskar", "Heronim",
            "Dariusz", "Alojzy", "Rafał", "Leszek", "Gustaw", "Jadwiga", "Emilia", "Diana", "Joanna", "Beata",
            "Edyta", "Julia", "Paulina", "Klaudia", "Monika");
    private static final List<String> LAST_NAMES = List.of("Wiśniewski", "Nowak", "Kołodziej", "Lis", "Szulc",
            "Baran", "Mazur", "Woźniak", "Bąk", "Sikora", "Cieślak", "Michalak", "Pawlak", "Walczak", "Duda");

    private static Player create(int numberLength, RandomNumbersGenerator randomNumbersGenerator) {
        Random random = new Random();
        String firstname = FIRST_NAMES.get(random.nextInt(FIRST_NAMES.size()));
        String lastname = LAST_NAMES.get(random.nextInt(LAST_NAMES.size()));
        return new Player(firstname, lastname, randomNumbersGenerator.generateUnique(numberLength));
    }

    public static List<Player> generateRandomPersons(int playersLimit, int numberLength, int maxNumberBound) {
        RandomNumbersGenerator randomNumbersGenerator =
                new RandomNumbersGenerator(new MathRandomNumber(), maxNumberBound);
        List<Player> players = new ArrayList<>();
        for (int i = 0; i < playersLimit; i++) {
            players.add(create(numberLength, randomNumbersGenerator));
        }
        return players;
    }
}
