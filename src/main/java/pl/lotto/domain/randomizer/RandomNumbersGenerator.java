package pl.lotto.domain.randomizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomNumbersGenerator {
    private final RandomNumber randomNumber;
    private final int numberBound;

    public RandomNumbersGenerator(RandomNumber randomNumber, int numberBound) {
        this.randomNumber = randomNumber;
        this.numberBound = numberBound;
    }

    public List<Integer> generateUnique(int amount) {
        Random random = new Random();
        return IntStream.generate(() -> random.nextInt(numberBound) + 1)
                .distinct()
                .limit(amount)
                .boxed()
                .collect(Collectors.toList());
    }
}
