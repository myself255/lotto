package pl.lotto.domain.randomizer;

public interface RandomNumber {
    int randomize(int min, int max);
}
