package pl.lotto.domain.game.reward;

import org.javamoney.moneta.Money;
import pl.lotto.domain.player.Player;

import java.util.List;

public class GameReward {
    private final Player player;
    private final String lottoGameId;
    private final Money reward;
    private final List<Integer> guessedNumbers;

    public GameReward(Player player, String lottoGameId, Money reward, List<Integer> guessedNumbers) {
        this.player = player;
        this.lottoGameId = lottoGameId;
        this.reward = reward;
        this.guessedNumbers = guessedNumbers;
    }

    public Player getPlayer() {
        return player;
    }

    public String getLottoGameId() {
        return lottoGameId;
    }

    public Money getReward() {
        return reward;
    }

    public List<Integer> getGuessedNumbers() {
        return guessedNumbers;
    }
}
