package pl.lotto.domain.game.reward;

import org.javamoney.moneta.Money;
import pl.lotto.domain.player.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class AmountRewardCalculator {
    private final Map<Grade, Money> gradeAccumulationRewards;

    public AmountRewardCalculator(Map<Grade, Money> gradeAccumulationRewards) {
        this.gradeAccumulationRewards = gradeAccumulationRewards;
    }

    public List<GameReward> rewards(List<Player> players, List<Integer> numbers, String lottoGameId) {
        Map<Grade, List<Player>> gradeRewards = new HashMap<>();
        players.forEach(player -> {
            List<Integer> playerSelectedNumbers = player.getSelectedNumbers();
            List<Integer> correctlyGuessedNumbers =
                    correctlyGuessedNumbers(playerSelectedNumbers, numbers);
            int numbersDifference = Math.abs(numbers.size() - correctlyGuessedNumbers.size());
            Grade grade = Grade.getGradeByDifferenceGuessingNumbers(numbersDifference);
            List<Player> playersInGrade = new ArrayList<>();
            if (gradeRewards.containsKey(grade)) {
                playersInGrade = gradeRewards.get(grade);
            }
            playersInGrade.add(player);
            gradeRewards.put(grade, playersInGrade);
        });

        List<GameReward> rewards = new ArrayList<>();
        gradeAccumulationRewards.forEach((grade, reward) -> {
            List<Player> playersInGrade = gradeRewards.get(grade);
            if (Objects.isNull(playersInGrade)) {
                return;
            }
            Money rewardPerPlayer = reward.divide(playersInGrade.size());
            playersInGrade.forEach(player -> rewards.add(new GameReward(player, lottoGameId, rewardPerPlayer,
                    correctlyGuessedNumbers(player.getSelectedNumbers(), numbers)
            )));
        });
        return rewards;
    }

    private List<Integer> correctlyGuessedNumbers(List<Integer> playerNumbers, List<Integer> randomNumbers) {
        List<Integer> guessedNumbers = new ArrayList<>();
        List<Integer> remainingRandomNumbers = new ArrayList<>(randomNumbers);
        for (Integer playerNumber : playerNumbers) {
            if (remainingRandomNumbers.contains(playerNumber)) {
                guessedNumbers.add(playerNumber);
                remainingRandomNumbers.remove(playerNumber);
            }
        }
        return guessedNumbers;
    }
}
