package pl.lotto.domain.game.reward;

import java.util.stream.Stream;

public enum Grade {
    GRADE_1(0), GRADE_2(1),
    GRADE_3(2), GRADE_4(3), NO_GRADE(4);
    private final int maxDifferenceInGuessingNumbers;

    Grade(int maxDifferenceInGuessingNumbers) {
        this.maxDifferenceInGuessingNumbers = maxDifferenceInGuessingNumbers;
    }

    public static Grade getGradeByDifferenceGuessingNumbers(int differenceGuessingNumber) {
        return Stream.of(Grade.values())
                .filter(grade -> grade.maxDifferenceInGuessingNumbers == differenceGuessingNumber)
                .findFirst()
                .orElse(NO_GRADE);
    }
}
