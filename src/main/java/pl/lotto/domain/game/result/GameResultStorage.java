package pl.lotto.domain.game.result;

public interface GameResultStorage {
    void save(GameResult gameResult);
}
