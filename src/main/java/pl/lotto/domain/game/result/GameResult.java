package pl.lotto.domain.game.result;

import pl.lotto.domain.game.LottoGame;
import pl.lotto.domain.game.reward.GameReward;

import java.time.LocalDateTime;
import java.util.List;

public class GameResult {
    private final List<Integer> numbers;
    private final List<GameReward> rewards;
    private final LottoGame lottoGame;
    private final LocalDateTime executionTime;

    public GameResult(List<Integer> numbers, List<GameReward> rewards, LottoGame lottoGame,
                      LocalDateTime executionTime) {
        this.numbers = numbers;
        this.rewards = rewards;
        this.lottoGame = lottoGame;
        this.executionTime = executionTime;
    }

    public List<Integer> getNumbers() {
        return numbers;
    }

    public List<GameReward> getRewards() {
        return rewards;
    }

    public LottoGame getLottoGame() {
        return lottoGame;
    }

    public LocalDateTime getExecutionTime() {
        return executionTime;
    }
}
