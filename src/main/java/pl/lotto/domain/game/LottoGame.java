package pl.lotto.domain.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.lotto.domain.game.result.GameResult;
import pl.lotto.domain.game.reward.AmountRewardCalculator;
import pl.lotto.domain.game.reward.GameReward;
import pl.lotto.domain.player.Player;
import pl.lotto.domain.randomizer.RandomNumbersGenerator;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

public class LottoGame {
    private static final Logger LOGGER = LoggerFactory.getLogger(LottoGame.class);
    private final String id;
    private final int numbersToSelect;
    private final RandomNumbersGenerator randomNumbersGenerator;
    private final AmountRewardCalculator amountRewardCalculator;
    private final Clock clock;

    public LottoGame(String id, int numbersToSelect, RandomNumbersGenerator randomNumbersGenerator,
                     AmountRewardCalculator amountRewardCalculator, Clock clock) {
        this.id = id;
        this.numbersToSelect = numbersToSelect;
        this.randomNumbersGenerator = randomNumbersGenerator;
        this.amountRewardCalculator = amountRewardCalculator;
        this.clock = clock;
    }

    public GameResult run(List<Player> players) {
        List<Integer> randomSelectedNumbers = randomNumbersGenerator.generateUnique(numbersToSelect);
        LOGGER.debug("Lotto ({}) has generated numbers: {}", id, randomSelectedNumbers.toString());
        List<GameReward> rewards = amountRewardCalculator.rewards(players, randomSelectedNumbers, id);
        LOGGER.debug("Lotto ({}) has calculated {} rewards", id, rewards.size());
        return new GameResult(randomSelectedNumbers, rewards, this, LocalDateTime.now(clock));
    }
}
