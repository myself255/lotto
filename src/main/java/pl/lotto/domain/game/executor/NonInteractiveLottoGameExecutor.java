package pl.lotto.domain.game.executor;

import pl.lotto.domain.game.executor.LottoGameExecutor;
import pl.lotto.domain.game.result.GameResultStorage;
import pl.lotto.domain.game.reward.AmountRewardCalculator;
import pl.lotto.domain.player.Player;
import pl.lotto.domain.randomizer.RandomNumber;

import java.time.Clock;
import java.util.List;

public class NonInteractiveLottoGameExecutor extends LottoGameExecutor {
    public NonInteractiveLottoGameExecutor(GameResultStorage gameResultStorage, int numbersToSelect, Clock clock,
                                           RandomNumber randomNumber, int numberMaxLimit,
                                           AmountRewardCalculator amountRewardCalculator, List<Player> players) {
        super(gameResultStorage, numbersToSelect, clock, randomNumber, numberMaxLimit, amountRewardCalculator, players);
    }

    @Override
    public void execute() {
        runGame();
    }
}
