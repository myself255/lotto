package pl.lotto.domain.game.executor;

import pl.lotto.domain.game.executor.LottoGameExecutor;
import pl.lotto.domain.game.result.GameResult;
import pl.lotto.domain.game.result.GameResultStorage;
import pl.lotto.domain.game.reward.AmountRewardCalculator;
import pl.lotto.domain.player.Player;
import pl.lotto.domain.randomizer.RandomNumber;
import pl.lotto.ui.UserInputInterface;

import java.time.Clock;
import java.util.List;

public class ConsoleLottoGameExecutor extends LottoGameExecutor {
    private final UserInputInterface userInputInterface;

    public ConsoleLottoGameExecutor(GameResultStorage gameResultStorage, int numbersToSelect, Clock clock,
                                    RandomNumber randomNumber, int numberMaxLimit,
                                    AmountRewardCalculator amountRewardCalculator, List<Player> players,
                                    UserInputInterface userInputInterface) {
        super(gameResultStorage, numbersToSelect, clock, randomNumber, numberMaxLimit, amountRewardCalculator, players);
        this.userInputInterface = userInputInterface;
    }


    @Override
    public void execute() {
        userInputInterface.welcomeDialog();
        Player player = userInputInterface.createPlayer(numbersToSelect, numberMaxLimit);
        players.add(player);
        GameResult gameResult = runGame();
        userInputInterface.notifyResult(gameResult);
    }
}
