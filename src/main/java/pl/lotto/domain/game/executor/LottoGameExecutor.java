package pl.lotto.domain.game.executor;

import pl.lotto.domain.game.LottoGame;
import pl.lotto.domain.game.result.GameResult;
import pl.lotto.domain.game.result.GameResultStorage;
import pl.lotto.domain.game.reward.AmountRewardCalculator;
import pl.lotto.domain.player.Player;
import pl.lotto.domain.randomizer.RandomNumber;
import pl.lotto.domain.randomizer.RandomNumbersGenerator;

import java.time.Clock;
import java.util.List;
import java.util.UUID;

public abstract class LottoGameExecutor {
    private final GameResultStorage gameResultStorage;
    private final LottoGame lottoGame;
    protected final int numberMaxLimit;
    protected int numbersToSelect;
    protected final List<Player> players;

    public LottoGameExecutor(GameResultStorage gameResultStorage,
                             int numbersToSelect, Clock clock, RandomNumber randomNumber, int numberMaxLimit,
                             AmountRewardCalculator amountRewardCalculator, List<Player> players) {
        this.gameResultStorage = gameResultStorage;
        this.numbersToSelect = numbersToSelect;
        this.numberMaxLimit = numberMaxLimit;
        this.players = players;
        this.lottoGame = new LottoGame(UUID.randomUUID().toString(), numbersToSelect,
                new RandomNumbersGenerator(randomNumber, numberMaxLimit), amountRewardCalculator, clock);
    }

    public abstract void execute();

    protected GameResult runGame() {
        GameResult gameResult = lottoGame.run(players);
        gameResultStorage.save(gameResult);
        return gameResult;
    }
}
