package pl.lotto;

import org.apache.log4j.BasicConfigurator;
import org.javamoney.moneta.Money;
import pl.lotto.infrastructure.Argument;
import pl.lotto.infrastructure.builder.lotto.LottoBuilder;

import javax.money.Monetary;
import java.time.Clock;
import java.time.ZoneId;
import java.util.Map;

import static pl.lotto.infrastructure.Argument.parseArguments;

public class App {
    private static final Clock CLOCK = Clock.system(ZoneId.of("Europe/Warsaw"));

    public static void main(String[] args) {
        BasicConfigurator.configure();
        Map<Argument, String> parsedArgs = parseArguments(args);

        LottoBuilder.builder(CLOCK)
                .selectInterfaceType(parsedArgs.get(Argument.INTERFACE))
                .selectGeneratedNumberLength(Integer.parseInt(parsedArgs.get(Argument.LENGTH)))
                .selectMaxNumberLimit(Integer.parseInt(parsedArgs.get(Argument.LIMIT)))
                .selectAccumulation(Money.of(Long.parseLong(parsedArgs.get(Argument.ACCUMULATION)),
                        Monetary.getCurrency("PLN")))
                .selectGradesRatio(
                        Double.parseDouble(parsedArgs.get(Argument.GRADE1)),
                        Double.parseDouble(parsedArgs.get(Argument.GRADE2)),
                        Double.parseDouble(parsedArgs.get(Argument.GRADE3)),
                        Double.parseDouble(parsedArgs.get(Argument.GRADE4)))
                .selectAmountOfPlayers(Integer.parseInt(parsedArgs.get(Argument.PLAYERS)))
                .build()
                .execute();
    }
}
