package pl.lotto.ui.console;

import pl.lotto.domain.game.result.GameResult;
import pl.lotto.domain.player.Player;
import pl.lotto.ui.UserInputInterface;

import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class ConsoleUI implements UserInputInterface {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private final MonetaryAmountFormat moneyFormat = MonetaryFormats.getAmountFormat(Locale.forLanguageTag("PL"));
    private final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    private final ScannerService scannerService;

    public ConsoleUI() {
        this.scannerService = new ScannerService();
    }

    @Override
    public Player createPlayer(int numbersToSelect, int numberMaxLimit) {
        System.out.println("Write your firstname: ");
        String firstname = scannerService.getString();
        System.out.println("Write your lastname: ");
        String lastname = scannerService.getString();
        System.out.printf("Select %d numbers from 1 to %d\n", numbersToSelect, numberMaxLimit);
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i <= numbersToSelect; i++) {
            System.out.printf("Write no.%d: ", i);
            int number = scannerService.getInt();
            if (number < 1 || number > numberMaxLimit || numbers.contains(number)) {
                System.out.println("Invalid range or duplicated number. Try again.");
                i--;
                continue;
            }
            numbers.add(number);
        }
        return new Player(firstname, lastname, numbers);
    }

    @Override
    public void welcomeDialog() {
        System.out.println("Welcome in game \"lotto\". Please, write your firstname, lastname and lucky numbers to " +
                "join lottery");
    }

    @Override
    public void notifyResult(GameResult gameResult) {
        System.out.println("Lottery has been finished!");
        String luckyNumbers = gameResult.getNumbers().toString();
        String durationTime = timeFormatter.format(gameResult.getExecutionTime());
        String reward = "Name: %s, numbers: %s, price: %s\n";
        String rewards = gameResult.getRewards().stream()
                .map(gameReward -> {
                    String name = gameReward.getPlayer().getFirstname() + " " + gameReward.getPlayer().getLastname();
                    return String.format(reward, name,
                            guessedNumbers(gameReward.getPlayer().getSelectedNumbers(), gameResult.getNumbers()),
                            moneyFormat.format(gameReward.getReward()));
                }).collect(Collectors.joining());
        System.out.printf("\nLucky numbers: %s, Duration time: %s\nRewards: \n%s", luckyNumbers, durationTime, rewards);
    }

    private String guessedNumbers(List<Integer> playerNumbers, List<Integer> lottoNumbers) {
        String guessedNumbers = "";
        List<Integer> remainingLottoNumbers = new ArrayList<>(lottoNumbers);
        for (Integer playerNumber : playerNumbers) {
            if (remainingLottoNumbers.contains(playerNumber)) {
                guessedNumbers += ANSI_GREEN + playerNumber + ANSI_RESET + ", ";
                remainingLottoNumbers.remove(playerNumber);
            } else {
                guessedNumbers += ANSI_RED + playerNumber + ANSI_RESET + ", ";
            }
//            if (playerNumbers.get(i).equals(lottoNumbers.get(i))) {
//                guessedNumbers += ANSI_GREEN + playerNumber + ANSI_RESET + ", ";
//            } else {
//                guessedNumbers += ANSI_RED + playerNumber + ANSI_RESET + ", ";
//            }
        }
        return "[" + guessedNumbers.substring(0, guessedNumbers.length() - 2) + "]";
    }
}
