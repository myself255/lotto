package pl.lotto.ui.console;

import java.util.Scanner;

public class ScannerService {
    private final Scanner scanner = new Scanner(System.in);

    int getInt() {
        return scanner.nextInt();
    }

    String getString() {
        return scanner.nextLine();
    }
}
