package pl.lotto.ui;

import pl.lotto.domain.game.result.GameResult;
import pl.lotto.domain.player.Player;

public interface UserInputInterface {
    Player createPlayer(int numbersToSelect, int numberMaxLimit);

    void welcomeDialog();

    void notifyResult(GameResult gameResult);
}
